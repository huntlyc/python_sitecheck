#!/usr/bin/python
# Written for python 2.6 
import sys
import urllib2
import BaseHTTPServer

class SitePinger:
    """Pings a list of sites to make sure they're alive..."""
    def __init__(self):
        self.failed_sites = []        

    def check_sites(self):
        """Pings a list of sites, and then writes a summary"""
        #TODO: build in some sort of flat file list for easy editing.
        sites = ['http://www.huntlycameron.co.uk',
                 'http://www.scottmcgurk.net',
                 'http://www.soundlightandmagic.co.uk',
                 'http://www.evolvetraining.com']
        [self.__ping_site(site) for site in sites]
        self.__print_summary()

    def __ping_site(self, site):
        """Tries to get the site index page, adds site to failed list if 
           it cant be reached
        """
        print "Checking: %s" % site
        try:
            response = urllib2.urlopen(site, None, 10)
            if(response is None):
                self.__log_error(site, "Null response")
        except urllib2.HTTPError as httpError: #log bad site
            self.__log_error(site, '%d %s' % (httpError.code,
                                              BaseHTTPServer.BaseHTTPRequestHandler.responses[httpError.code]))
    
    def __log_error(self, site, reason):
        """Records the failed site and reason"""
        self.failed_sites.append((site, reason))

    def __print_summary(self):
        """Displays a list of the failed sites and the reason for failure"""
        print("\nSummary:\n==================================================")
        if len(self.failed_sites) == 0:
            print "All good"
        else:
            #Make the output a bit more friendlier
            num_failed = len(self.failed_sites)            
            if(num_failed == 1): 
                print "!! FAILED ON (1) site !!"
            else:
                print "!! FAILED ON (%d) sites !!" % num_failed
            

            print('--------------------------------------------------')
            #display the site and the reason
            for site,reason in self.failed_sites:
                print "%s - %s" % (site,reason)

        
def main(args):
    sitePinger = SitePinger()
    sitePinger.check_sites()

if __name__ == '__main__':
    sys.exit(main(sys.argv))

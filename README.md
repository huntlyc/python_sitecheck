Python Sitecheck
================

2am, sore back and nothing better to do than to play about with python ;)

Update
------

Tried out the same thing in ruby (in a seperate branch) to compare the two languages (even if 
it isn't the best or most comprehensive test in the world) and have
added that to the repo.

Update2
-------
The python script was origonally written in python3, but I've back-ported it to python 2
